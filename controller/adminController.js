const Category = require("../model/Category");
const Bank = require("../model/Bank");
const Item = require("../model/Item");

// 1

module.exports = {
  // Category
  // Menampilkan Category
  viewCategory: async (req, res) => {
    try {
      const category = await Category.find();
      res.send(category);
    } catch (error) {
      res.send("view gagal");
    }
  },
  // Menambahkan Category
  addCategory: async (req, res) => {
    // Validation
    const dataExist = await Category.findOne({
      categoryName: req.body.categoryName,
    });
    if (dataExist) return res.status(400).send("cannot add twin item");
    //

    const category = new Category({
      categoryName: req.body.categoryName,
    });
    try {
      await category.save(); //save category to db
      res.send({ category: category._id });
    } catch (error) {
      console.log(error);
      res.status(400).send(error);
    }
  },
  // Edit Category
  editCategory: async (req, res) => {
    try {
      const edited = await Category.findById(req.params.catId);
      edited.categoryName = req.body.categoryName;
      const result = await edited.save();
      res.json(result);
    } catch (error) {
      res.json("error");
    }
  },

  // Bank
  // Menampilkan Bank
  viewBank: async (req, res) => {
    try {
      const bank = await Bank.find();
      res.send(bank);
    } catch (error) {
      res.send("view gagal");
    }
  },

  // Menambahkan Bank
  addBank: async (req, res) => {
    // Validation
    const dataExist = await Bank.findOne({
      noRekening: req.body.noRekening,
    });
    if (dataExist) return res.status(400).send("cannot add twin item");
    //
    const bank = new Bank({
      nameBank: req.body.nameBank,
      noRekening: req.body.noRekening,
      name: req.body.name,
      imageUrl: req.file.filename,
    });

    try {
      await bank.save(); //save category to db
      res.send({ bank: bank._id });
    } catch (error) {
      console.log(error);
      res.status(400).send(error);
    }
  },

  // Edit Bank
  editBank: async (req, res) => {
    try {
      const edited = await Bank.findById(req.params.bId);
      edited.nameBank = req.body.nameBank;
      edited.noRekening = req.body.noRekening;
      edited.name = req.body.name;
      edited.imageUrl = req.file.filename;
      const result = await edited.save();
      res.json(result);
    } catch (error) {
      res.json("error");
    }
  },
  // Item
  // Menampilkan Item
  // Menampilkan Category
  viewItem: async (req, res) => {
    try {
      const item = await Item.find();
      res.send(item);
    } catch (error) {
      res.send("view gagal");
    }
  },
  // Menambahkan Item
  addItem: async (req, res) => {
    const categoryId = req.body.categoryId;
    const category = await Category.findOne({ _id: categoryId });
    const item = new Item({
      categoryId: category,
      itemName: req.body.itemName,
      price: req.body.price,
      description: req.body.description,
      imageUrl: req.file.filename,
    });
    category.itemId.push({ _id: item._id });
    try {
      await category.save();
      await item.save(); //save category to db
      res.send({ item: item._id });
    } catch (error) {
      res.send(error);
    }
  },
  // edit Item
  editItem: async (req, res) => {
    try {
      const edited = await Item.findById(req.params.iId);
      edited.itemName = req.body.itemName;
      edited.price = req.body.price;
      edited.description = req.body.description;
      edited.imageUrl = req.file.filename;
      const result = await edited.save();
      res.json(result);
    } catch (error) {
      res.json("error");
    }
  },
};
