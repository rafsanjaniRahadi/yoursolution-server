const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "images");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

// upload parametes
const upload = multer({
  storage: storage,
});

const uploadSingle = upload.single("imageUrl");

module.exports = { uploadSingle };

// img: req.file.filename
