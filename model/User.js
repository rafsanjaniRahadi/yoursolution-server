const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    min: 4,
    max: 255,
  },
  email: {
    type: String,
    required: true,
    max: 255,
  },
  password: {
    type: String,
    required: true,
    min: 5,
    max: 1000,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  // require image
  // require booking
});

module.exports = mongoose.model("User", userSchema);
