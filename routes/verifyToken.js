const jwt = require("jsonwebtoken");

// verify token
module.exports = function (req, res, next) {
  const token = req.header("auth-token"); //mengambil token dari header
  if (!token) return res.status(401).send("Access denied"); //null token == denied
  //there token run this
  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET); //function from jwt verify the token
    req.user = verified; //user got his status as verified
    next(); //pass verify session
  } catch (error) {
    res.status(400).send("Invalid Token");
  }
};
