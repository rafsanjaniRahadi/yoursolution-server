const router = require("express").Router();
const adminController = require("../controller/adminController");
// const { uploadSingle, uploadMultiple } = require("../middleware/multer");
const { uploadSingle } = require("../middleware/imageModule");

// Category
router.get("/category", adminController.viewCategory);
router.post("/category", adminController.addCategory);
router.patch("/category/:catId", adminController.editCategory);

// Bank
router.get("/bank", adminController.viewBank);
router.post("/bank", uploadSingle, adminController.addBank);
router.patch("/bank/:bId", uploadSingle, adminController.editBank);

// Item
router.get("/item", adminController.viewItem);
router.post("/item", uploadSingle, adminController.addItem);
router.patch("/item/:iId", uploadSingle, adminController.editItem);

module.exports = router;
