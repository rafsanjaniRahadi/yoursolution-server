const router = require("express").Router();
const User = require("../model/User");
const {
  registerValidation,
  loginValidation,
} = require("../controller/validation");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

// register
router.post("/register", async (req, res) => {
  //Validation before adding user
  const { error } = registerValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  // Checking user email
  const emailExist = await User.findOne({ email: req.body.email });
  if (emailExist) return res.status(400).send("email already exist");
  //  hashing password/enskripsi password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);
  // Create a new user
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashedPassword,
  });
  try {
    const savedUser = await user.save(); //save user to db
    res.send({ user: user._id });
  } catch (err) {
    res.status(400).send(err);
  }
});

// login
router.post("/login", async (req, res) => {
  // Validating to entry
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  // Checking user email
  const user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).send("email or password its not ok");
  // Password is Correct
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.status(400).send("email or password its not ok");

  // Create token
  const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET); //membuat token jwt untuk user yang login
  res.header("auth-token", token).send(token); //input token ke header auth-token
});

// resets password
router.patch("/resetPassword/:userId", async (req, res) => {
  try {
    const reset = await User.findById(req.params.userId);
    // reseting
    const reseted = req.body.password;
    // hashing password/enskripsi password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(reseted, salt);
    // passing to object
    reset.password = hashedPassword;
    // saving
    const result = await reset.save();
    res.json(result);
  } catch (error) {
    res.json("error");
  }
});

router.get("/view", async (req, res) => {
  try {
    const user = await User.find();
    res.send(user);
  } catch {
    res.send("tidak dapal menampilkan user");
  }
});

module.exports = router;
