// private part on website
// example
const router = require("express").Router();
const verify = require("./verifyToken");

router.get("/", verify, (req, res) => {
  res.json({
    posts: { title: "My example post for verify", description: "random data" },
  });
});

module.exports = router;
